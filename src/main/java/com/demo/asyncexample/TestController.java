package com.demo.asyncexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@Autowired
	private AsyncService myService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getHello() {
		myService.testAsync();
		return "Hello World";
	}

	@RequestMapping(value = "/async", method = RequestMethod.GET)
	@Async
	public String getHelloAsync() {
		try {
			Thread.sleep(25000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "Hello World";
	}

	@RequestMapping(value = "/nonasync", method = RequestMethod.GET)
	public String getNonAsync() {
		myService.testNonAsync();
		return "Hello World";
	}

}
