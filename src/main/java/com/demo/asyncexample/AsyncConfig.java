package com.demo.asyncexample;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfig {
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Bean
    public Executor taskExecutor() {
        if (threadPoolTaskExecutor == null) {
            threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
            threadPoolTaskExecutor.setCorePoolSize(200);
            threadPoolTaskExecutor.setMaxPoolSize(1000);
            threadPoolTaskExecutor.setQueueCapacity(100000);
            threadPoolTaskExecutor.setThreadNamePrefix("TestingAsync-");
            threadPoolTaskExecutor.initialize();
        }
        return threadPoolTaskExecutor;
    }
}
